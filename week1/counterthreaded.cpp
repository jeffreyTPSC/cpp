#include <iostream>
#include <thread>

void f1() { std::cout << "Dit is functie 1. \n"; }
void f2() { std::cout << "Dit is functie 2. \n" ;}

int main()
{
	std::thread it1(f1), t2(f2);

	//threads joinen
	t1.join();
	t2.join();
}

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

using namespace std;


int main()
{
        const int MAX = 11;
        int child1,child2;
        child1=fork();
        if (child1==0)
        {
        printf("Dit is child1 met pid %d \n",getpid());
        for ( int counter = 1; counter < MAX; counter++ )
                {
                cout << counter << " (1)\n";
		sleep(1);
                }
        }
        else
                {
                child2=fork();
                if(child2==0)
                        {
                                printf("Dit is child2 met pid %d \n",getpid());
                                for ( int counter = 1; counter < MAX; counter++ )
                                cout << counter << " (2)\n";
				sleep(1);
                        }
                }
        printf("De parent heeft pid %d \n",getpid());
	return 0;
}

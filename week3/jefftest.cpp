#include <iostream>
#include <fstream>

using namespace std;
/*
struct HuffmanTree {
char x;
int x_aantal;
HuffmanTree *links;
HuffmanTree *rechts;
}
*/

int main() {
	NodePtr huffman = bouw_tree();
	print_tree(huffman);

	free_memory(huffman);

	return 0;
}

NodePtr bouw_tree() {

	/* Invoer van text file even disabled om huffman tree(binary tree) te maken op simpel manier eerst.

	char str[100];
	cout << "Voer naam van bestaande text file in:"
	cin.get (str,100);
	ifstream is(str);
	

	char c;
	while ( is.get(c))
		cout << c;
		is.close;
		return 0;
	*/
	
	int karakters;
	cin >> karakters;
	char letter;
	int frequentie;
	HuffmanPriorityQueue queue;
	NodePtr p;
	for (int i = 0; i < karakters; i++) {
	NodePtr temp = new HuffmanNode(frequentie, letter);
	queue.push(temp);
	}

	for (int i = 0; i < karakters -1; i++){
	NodePtr a = queue.top();
	queue.pop();
	NodePtr b = queue.top();
	NodePtr p = new HuffmanNode(a,b);
	queue.push(p);
	}
return queue.top();
}

void print_tree(NodePtr root, int indent = 0, string prefix = "")
{
    // External nodes are not printed.
    if (root == NULL) {
        return;
    }

    char letter = ' ';
    if (root->is_leaf()) {
        letter = root->letter;
    }

    cout << string(indent, ' ') << prefix << "(" << letter << " [" << root->frequency << "]";
    if (root->is_leaf()) {
        cout << ")" << endl;
    } else {
        cout << endl;
        // Print left and right subtrees with the appropriate prefix, and
        // increased indent (by INDENT_SIZE).
        print_tree(root->left, indent + INDENT_SIZE, "0");
        print_tree(root->right, indent + INDENT_SIZE, "1");
        cout << string(indent, ' ') << ")" << endl;
    }
}
